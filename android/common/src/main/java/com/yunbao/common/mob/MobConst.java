package com.yunbao.common.mob;

import com.yunbao.common.Constants;

import java.util.HashMap;
import java.util.Map;

import cn.sharesdk.facebook.Facebook;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.twitter.Twitter;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------

public class MobConst {

    public static final Map<String, String> MAP;

    static {
        MAP = new HashMap<>();
        MAP.put(Constants.MOB_QQ, QQ.NAME);
        MAP.put(Constants.MOB_QZONE, QZone.NAME);
        MAP.put(Constants.MOB_WX, Wechat.NAME);
        MAP.put(Constants.MOB_WX_PYQ, WechatMoments.NAME);
        MAP.put(Constants.MOB_FACEBOOK, Facebook.NAME);
        MAP.put(Constants.MOB_TWITTER, Twitter.NAME);
    }

}
